import 'package:flutter/material.dart';
import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'main.dart';


class PageTwo extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: Scaffold(
      // backgroundColor: Colors.cyan,
      //
      // appBar: AppBar(
      // title: Text("Yoda's Travel"),
      // centerTitle: true,
      // backgroundColor: Colors.indigo,
      // elevation: 0,
      // ),
      home: MyHomePage(title: "Yoda's Travel",

      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

enum Answers{YES,NO,MAYBE}
class _MyHomePageState extends State<MyHomePage> {

  String _value = '';

  void _setValue(String value) => setState(() => _value = value);

  Future _askUser() async {
    switch(
    await showDialog(

        context: context,useRootNavigator: true,
        /*it shows a popup with few options which you can select, for option we
        created enums which we can use with switch statement, in this first switch
        will wait for the user to select the option which it can use with switch cases*/
        child: new SimpleDialog(
          title: new Text('Our Services').tr(),
          children: <Widget>[
            new SimpleDialogOption(child: new Text('Train.').tr()
            ),
            new SimpleDialogOption(child: new Text('Bus.').tr()),
            new SimpleDialogOption(child: new Text('Flight.').tr()),
          ],
        )
    )
    ) {
      case Answers.YES:
        _setValue('Train.');
        break;
      case Answers.NO:
        _setValue('Bus.');
        break;
      case Answers.MAYBE:
        _setValue('Flight.');
        break;
    }
  }

  final globalKey= GlobalKey<ScaffoldState>();
  Future<String> createAlertDialog(BuildContext context){

    TextEditingController customController =TextEditingController();
    return showDialog(context: context,builder:(context){
      return AlertDialog(
        title: Text("Please enter your Name to agree on wearing mask and sanitizing your hands for your travel?").tr(),
        content:
        TextField(
          controller:customController ,
        ),
        actions:<Widget> [
          MaterialButton(
            elevation: 5.0,
            child: Text('Submit').tr(),
            onPressed: (){

              Navigator.of(context, rootNavigator: true).pop(customController.text.toString());


            },
          )
        ],
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      key: globalKey,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body:
      Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.

        child:
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height:10),
            Text("Thank you for choosing Yoda's Travel",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.indigo,
                fontSize: 30,
              ),).tr(),
            SizedBox(height:20),

            Container(
              child: Image.asset('assets/images/starter-image.jpg',
                fit: BoxFit.cover,
                height: 300,
                width: 400,
              ),
            ),
            Container(
              // margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child:
                FlatButton(
                  onPressed: ()
                  {
                    createAlertDialog(context).then((onValue) {
                      if(onValue!=null && onValue!="") {
                           final snackBar = SnackBar(content: Text("Hello $onValue, You have agreed to take necessary safety precautions!").tr());
                           globalKey.currentState.showSnackBar(snackBar);

}
                      }
                    );
                  },
                  child: Text('Covid-19 Alert Consent').tr(),
                  textColor: Colors.white,
                  color: Colors.red,
                  padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                )
            ),

            Container(
              child:  Center(
                child:  Column(

                  children: <Widget>[
                    RaisedButton(onPressed: _askUser,

                      child: new Text('Services',
                        style: TextStyle(
                          color: Colors.white,
                        ),).tr(),
                      color: Colors.redAccent,)
                  ],
                ),

              ),
            ),

    Container(
              //    margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child:
                RaisedButton(
                  onPressed: () =>
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => new MyApp())),
                  child: Text('Back').tr(),
                  textColor: Colors.white,
                  color: Colors.red,
                  padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                )
            ),

          ],

        ),
      ),
    );
  }
}
